from django.db import models

# Create your models here.
class Customers(models.Model): 
    fname = models.CharField(max_length = 255)
    lname = models.CharField(max_length = 255)
    school = models.TextField()
    year = models.TextField()
    department = models.TextField()
    phone = models.TextField()
    email = models.EmailField()

class Images(models.Model):
    location = models.TextField()

class Projects(models.Model):
    name = models.CharField(max_length = 255)
    language = models.TextField()
    LOC = models.TextField()
    imgID = models.ForeignKey('Images',on_delete=models.CASCADE)
    video = models.TextField()
    Description = models.TextField()
    price = models.TextField()

class Message(models.Model):
    subject = models.CharField(max_length = 255)
    body = models.TextField()
    custID = models.ForeignKey('Customers',on_delete=models.CASCADE)
    date = models.TextField()



class Buy(models.Model):
    projectID = models.ForeignKey('Projects',on_delete=models.CASCADE)
    custID = models.ForeignKey('Customers',on_delete=models.CASCADE)

