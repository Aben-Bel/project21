from django.contrib import admin

# Register your models here.
from .models import Projects
from .models import Images
from .models import Buy
from .models import Customers
from .models import Message

admin.site.register(Projects)
admin.site.register(Images)
admin.site.register(Customers)
admin.site.register(Message)
admin.site.register(Buy)